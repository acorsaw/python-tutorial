# Lesson 2
Just like last time you'll need your editor. If you forgot what the icon looks
like here's a reminder:
![Atom.io Icon](images/atom-icon.png)

You will be making your own Python script today instead of having one ready to
go. You will probably want to still save it to the `C:\var\python` directory so
that you can remember where it's at. So, make a new file and let's try out some
different python code today.

Start with something basic and make your program print something when you run
it. If you forgot how to do that you can open your old hello-world.py file and
see how to make Python print something.

Now we will talk about variables. `Variables` sounds like an unusual word that
you wouldn't hear in your typical day but in programming you use it all time.
Basically all a variable does is give you a place to store something and a name
to call it. For example, if I ask you to bring my your toothbrush you would know
exactly what to bring me and exactly where it was because you have given it a
name and you have a place to store it. Here are a couple different ways to use
some variables:

```
name = "William Blazkowicz"
```
In this case I have a variable called `name` and it is storing the value of
someone's name.

```
age = 23
```
This time there are no quotes around it because it's just a number. Python
treats numbers and text (sometimes called "strings") differently. Let's make
an example so that we can see exactly what that means.

```
first = "William"
last = "Blazkowicz"
full = first + last
```

Print out the full name now and see what Python does for you. For extra points
see if you can make the full name print out correctly and not just smushed
together text. I can think of at least three different ways to do it with just
what you know so far. Can you find one (or three, or maybe one that I didn't
think of)?

Now lets get a little more complicated. We will build an entire greeting with
some information we know.
```
first = "William"
last = "Blazkowicz"
full = first + last
age = 23

print("Hi, my name is " + full + " and I am " + 23 " years old.")
```

If you run that what do you get as the output? It probably looks something like
this `TypeError: must be str, not int`. What the heck does that mean? Well, it
will tell you what line the error is on, do you see that part? It doesn't like
the line that you have the print function on. It is because it is mixing numbers
and text (or strings) and it doesn't know for sure what you want to do with that
so lets be more specific. We will convert the number to a string (I didn't call
it text here, I wonder why....).

```
print("Hi, my name is " + full + " and I am " + str(23) + " years old.")
```

That works way better! The Python built in function `str()` will convert a
number to a string. Then, now that everything is a string, the print function
is able to put it all together and produce the correct output. One last thing
before we finish up with printing, strings, integers and conversions.

Wouldn't it be nice to have a way to print all that without having to put so
many plus signs in it? Well, if you REALLY want to get fancy, I'll show you.
Try this and see what you think:
```
print('Hi, my name is {} {} and I am {} years old'.format(first, last, age))
```

Well, that's interesting. Let's see what that really does. We can break that out
into two pieces.
```
greeting = 'Hi, my name is {} {} and I am {} years old'.format(first, last, age)
print(greeting)
```

Is that a little more clear? It is taking a string with placeholders in it and
then putting the replacements in place of the placeholders. That's interesting
but you probably don't think its all that useful. We'll come back to that in a
later lesson and you may change your mind (insert evil grin here).

So, to summarize what you learned today you now know about Python variable
types (or at least two of them), string concatenation (that's the programmer
word for taking one thing and putting something else on the end of it), and string
formatting using placeholders.

