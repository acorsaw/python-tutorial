# Lesson 1
First things first. You have to have some place to write some code. Start up the
Atom editor (I already installed it on all the computers). If you have any
problems finding it you can look for an icon like this:

![Atom.io Icon](images/atom-icon.png)

Once you have the editor open it should look like this:

![Atom Editor](images/atom-blank.png)

If it doesn't look exactly like this don't worry about it. It keeps track of
what you were working on last time so it will always look a little different
when you open it.

Great! Now that you have the editor open you should be able to open a file. On
your "C" drive there is a file you need to open. It's

```
C:\var\python\hello-world.py
```

so using the Atom editor open the file and see what it says.

That's pretty much the most simple Python program you can write but its a good
place to start. I have already installed both Atom, Python, and a plug-in for
Atom that lets it run Python programs and show you the output. If you press the
"F5" key you should see output something like this:

 ![Python console output](images/python-console.png)

 __Sweet!__ The Python interpretor read the file and printed the text
"Hello World!" to the console. It didn't see anything else to do so it quit. If
you press any key (space, Enter, or whatever) the console window (the black
screen) will close and you will be back in the Atom editor again. See what
else you can get Python to print. Can you edit the file to get it to do more
than just print "Hello World!".

