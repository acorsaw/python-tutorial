# Lesson 3
Today we will be working on some functions. You have already been working
with some functions (like print( ) for example) but now you'll make some of
your own.

In your last program you had it print out a greeting to a specific person. Wouldn't
it be nice if you could just teach the computer to say hi and it would know what
to do? Well, you can!

Lets start talking a little bit about how Python knows you want to create a
function. We will start with some functions and work backwards to explain
what it is doing.

```
def cook_food(raw_food):
  print("I just cooked your " + raw_food)

  def eat_food(cooked_food):
    print("Yum! I'm glad " + cooked_food + "wasn't raw!")
    ```

    There are two functions here, cook_food and eat_food. You can tell they are functions
    because they start with `def`. The word that follows it is the name of the function
    and then some parenthesis. The words inside the parenthesis are called parameters
    (sometimes they are called arguments in other languages). These can be used as
    variables in your function.

    After the parenthesis there is a colon. This tells Python that you're ready to begin your
    function. The next line is indented (that's just a fancy word to say that it is pushed
    in compared to the other lines). This is __REALLY__ important in Python. The
    indentation tells Python that the things coming next that are indented the same
    amount are all part of the same block of code. After the function is over you
    can see that the indentation returns to normal.

    The inner part of the function that is indented should look really familiar. Its
    your old friend the __print__ function. You can see that its using the variable
    that you declared in the function (raw_food in the first function and
    cooked_food in the second function).

    Great, now you can make things that do stuff but these functions are kind of
    greedy. You give them things but they never give anything back. We can fix that
    though.

    ```
    def add(addend1, addend2):
      sum = addend1 + addend2
        return sum

	first = 5
	second = 4
	sum = add(first, second)
	print("The sum of {} and {} is {}".format(first, second, result))
	```

	Well, don't freak out because that's a lot more code than you're used to seeing
	at once. We can walk through it. You define the function called add and specify
	that it will have two parameters addend1 and addend2 `def add(addend1, addend2):`
	Then you declare a variable called sum that holds the result of adding the two
	variables together `sum = addend1 + addend2` After that you use a new keyword
	that we haven't used before, `__return__`. The `return` statement takes one
	paremeter and will set the variable in the section of code that called it. I know
	that sounds pretty technical but it just means that you can use the function to
	set a variable.

	This line `sum = add(first, second)` is where the cool stuff happens. Remember
	when you would set a variable to a string like `first = "William"` in your earlier
	program? This is doing the same thing. It calls the add function and returns
	the result of that into your new variable.

	You're probably tired of seeing my code, lets get you writing some of your own!
	Since you've see a function and you know how they work start with a new file
	of your own. First create a function named `say_hello` that takes two parameters
	called `first_name` and `last_name`. It should return a greeting to the person
	that has the first name and last name that you passed in, probably something
	like "Hello, Mr. Blazkowicz! May I call you William or do you prefer another name?"

	After you create the function you need to use it so call the function and then print
	the resulting greeting. Since today is about creating the function I'll help out
	and give you the rest of the program.

	```
	greeting = say_hello(first_name, last_name)
	print(greeting)
	```

	Ok, so it wasn't that much help since you did all the work in the function but
	at least I can say that I helped some :D

	After you get done with today's lesson you now know about variables, different
	types of variables, functions, parameters, and return values. I think you should
	feel pretty good about that so far!
