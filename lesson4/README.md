# Lesson 4

Ok, so you can write your own functions now so that's pretty awesome but I bet
you were thinking "Hey, I would think that someone else has already made a lot
of functions before. Why would I have to write them all over again? They should
share!" Well, you're right! There are lots of stuff that come with Python and there's
a ton more that you can get really easily if you need it. Here are a list of the
[functions](https://docs.python.org/3/library/functions.html) that are built in to Python.
Some of them should look very familiar to you already. Do you see your old
friends `print`, `format`, and `str`?

Let's just go over a few of the cool ones:
* sum: This would have been handy in the previous lesson. It will add an unlimited
number of numbers together.
* min: This will return the smalled item in a list or the smallest of two parameters
that you pass in.
* reversed: This will return an iterator that will reverse the order of a list (The
list will start at the end and end at the beginning)
* sorted: This will return a new list that is sorted in normal order. If you want
you can even have the list sorted in reverse order if you set the reversed
parameter to True.

For some practice lets use the `max` function. Its a pretty cool function that can
work in a variety of ways. You can either pass it a list or just a bunch of parameters.

Either this:
```
max_value = max(8, 1, 2, 5, 6)
```

Or you can create a list and pass it a list of values.
```
values = ["Adam", "Grace", "Jacob", "Mindy"]
max_name = max(values)
```

Pick out a built in Python function and create an example of how you are going to use it.
