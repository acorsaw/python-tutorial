# Lesson 5

# Fun with Lists

In the last lesson we got to work with Python lists. We had a list of things
and we used the functions `min` and `max` to get either the biggest or
smallest thing out of the list. In this lesson we will do some more advanced
things with lists like using a list to make new lists and doing something on
all the things in a list.

Let's start out with a simple list of numbers:

```
diameter_list = [1, 2, 3, 4]
```

If you wanted to do something with each number in the list there are a couple
different things we could do. Let's just say that each number is the diameter
of a circle and we want to multiply each number times Pi (3.14) so that we can
calculate the circumference of the circle. In english it would look something
like this:

```
For every number in the list multiply it by 3.14 and print out the resulting
number.
```

If we write it in Python we get to use a lot less words:

```
for diameter in diameter_list:
    print(diameter * 3.14)
```

If you put that in your program you should get some output that looks like
this (don't forget to create your list before you start your `for` loop):
```
3.14
6.28
9.42
12.56
```

Neat! Nearly every language has a `for` loop in it because it's so useful. The
`for` loop in Python is much easier to read but let's break it down anyway so
that we know exactly how to read it when we look at the code:

```
for diameter in diameter_list:
```
This says a lot in just a little bit of space. In english it says "For each
diameter in the diameter_list do the things that are indented below after the
colon." It is creating a diameter variable that you can work with in the
indented code below the colon. After you are done using the `diameter` variable
it will get cleaned up for you automatically.

The next thing is does is this:
```
    print(diameter * 3.14)
```

This is your old friend the print function and it is using your `diameter`
variable that you declared in your `for` loop to do some math.

Python has some more neat stuff to make our code even easier to write but
it's a little trickey for some people. If this looks strange the first time
you see it don't worry about it. You'll see it more later.

```
diameter_list = [1, 2, 3, 4]
[ print(diameter * 3.14) for diameter in diameter_list ]
```

This is called a list comprehension and its one of the neat features of
Python. It basically says:
```
[ **DO THIS** for **EACH ITEM** in **LIST OF ITEMS** ]
```

And the neat thing is that it will return a new list that you can work with
when you're done. Lets say you have a list of radius values for circles like
this:
```
radius_list = [ 1, 2, 3, 4 ]
```

To get a list of diameters you can just multiply each of them by 2 right?
Using the list comprehension we can just do this to get our diameter list:
```
radius_list = [ 1, 2, 3, 4 ]
diameter_list = [ radius * 2 for radius in radius_list ]
print("Diameter List: ", diameter_list)
```

If you run that you can see that you now have a radius list and a diameter
list that you will be able to use later on. Do you remember the formula for
area of a circle? `area = Pi * R^2` In computer math that's area equals Pi
time radius squared. And you know that radius squared is just radius times
radius right? So, really it's `area = Pi * radius * radius`.

Do you think you can make a list of the areas of a circle given a list of
the radius values? You can use part of the example above where we computed
the diameter as a template. Try it out and see how things go.